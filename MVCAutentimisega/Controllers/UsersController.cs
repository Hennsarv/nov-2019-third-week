﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisega.Models;

namespace MVCAutentimisega.Models
{
    partial class AspNetUser
    {
        public static AspNetUser ByEmail(string email)
        {
            UsersEntity db = new UsersEntity();
            return db.AspNetUsers.Where(x => x.Email == email).SingleOrDefault();
        }
    }
}

namespace MVCAutentimisega.Controllers
{
    public class UsersController : Controller
    {
        private UsersEntity db = new UsersEntity();

        // GET: AspNetUsers
        public ActionResult Index()
        {
            return View(db.AspNetUsers.ToList());
        }
        [Authorize(Roles = "Administrator")]
        public ActionResult AddRole(string id, string roleId)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            AspNetRole r = db.AspNetRoles.Find(roleId);
            if (u != null && r != null)
            {
                try
                {
                    u.AspNetRoles.Add(r);
                    db.SaveChanges();
                }
                catch (Exception)
                {

                }
            }
            //            return View("Details", u);
            return RedirectToAction("Details", new { id = id });
        }
        [Authorize(Roles = "Administrator")]
        public ActionResult RemoveRole(string id, string roleId)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            AspNetRole r = db.AspNetRoles.Find(roleId);
            if (u != null && r != null)
            {
                try
                {
                    u.AspNetRoles.Remove(r);
                    db.SaveChanges();
                }
                catch (Exception)
                {

                }
            }
            //            return View("Details", u);
            return RedirectToAction("Details", new { id = id });
        }

        // GET: AspNetUsers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.Roles = db.AspNetRoles.ToList()
                .Except(aspNetUser.AspNetRoles.ToList())
                .ToList();


            return View(aspNetUser);
        }

        // GET: AspNetUsers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AspNetUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DisplayName,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,BirthDate,Lisainfo,Veelmidagi,FirstName,LastName")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                db.AspNetUsers.Add(aspNetUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aspNetUser);
        }

        // GET: AspNetUsers/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: AspNetUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DisplayName,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,BirthDate,Lisainfo,Veelmidagi,FirstName,LastName")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aspNetUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aspNetUser);
        }

        // GET: AspNetUsers/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: AspNetUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(aspNetUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
