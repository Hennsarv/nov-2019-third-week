﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConsoleDB
{
    class Program
    {
        static void Main(string[] args)
        {
            // ODBC / OLEDB
            // Connection to database
            // Command (e päring e query)
            // DataReader vastus
            //SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Henn\Northwind.mdf;Integrated Security=True;Connect Timeout=30");
            SqlConnection conn = new SqlConnection("Data Source=valiitsql.database.windows.net;Initial Catalog=Northwind;Persist Security Info=True;User ID=student;Password=Pa$$w0rd");
            conn.Open();
            SqlCommand comm = new SqlCommand("select ProductName, UnitPrice from products", conn);
            var R = comm.ExecuteReader();
            while(R.Read())
            {
                Console.WriteLine($"{R["ProductName"]} hinnaga {R["UnitPrice"]}");
            }
        
        }
    }
}
