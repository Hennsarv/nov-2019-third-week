﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEntityFramework
{
    partial class Employee
    {
        public string FullName => $"{FirstName} {LastName}";
    }


    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities ne = new NorthwindEntities();

            #region commenteerisin välja
            //ne.Database.Log = Console.WriteLine;

            //foreach (var p in ne.Products
            //    .Select(x => new { x.UnitPrice, x.ProductName })
            //    .Where(x => x.UnitPrice > 100)
            //    .OrderBy(x => x.UnitPrice)

            //    ) Console.WriteLine(p.ProductName);

            //Console.WriteLine(
            //ne.Categories.Find(8).CategoryName
            //);
            //ne.Categories.Find(8).CategoryName = "Seafood";
            //Console.WriteLine(
            //ne.Categories.Find(8).CategoryName
            //);
            //ne.SaveChanges();

            ////Product uus = new Product { ProductName = "Mängukaru", UnitPrice = 10, CategoryID = 8 };
            ////ne.Products.Add(uus);
            ////ne.SaveChanges();

            //ne.Categories.Find(1).Products.Add(new Product { ProductName = "lutsukomm", UnitPrice = 0.5M });
            //ne.SaveChanges();

            //foreach (var p in ne.Products.Where(x => x.ProductName.StartsWith("lutsu"))) ne.Products.Remove(p);
            //ne.SaveChanges(); 
            #endregion

            foreach (var e in ne.Employees)
                Console.WriteLine($"{e.FullName} ülemus on {e.Manager?.FullName??"ta on ise jumal"}");

            //ne.Database.Log = Console.WriteLine;

            foreach (var x in ne.Categories
                  //.Include("Products")
             )
            {
                Console.WriteLine(x.CategoryName);
                foreach (var y in x.Products)
                    Console.WriteLine("\t" + y.ProductName);
            }

            var q1 = from p in ne.Products
                     where p.UnitPrice > 100
                     orderby p.UnitPrice descending
                     select new { p.ProductName, p.UnitPrice, p.Category.CategoryName};
            foreach (var p in q1) Console.WriteLine(p);

            var q2 = from c in ne.Categories
                     from p in c.Products
                     select new { c.CategoryName, p.ProductName };
            foreach (var p in q2) Console.WriteLine(p);


        }
    }
}
