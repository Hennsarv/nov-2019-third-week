﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CustomeriAken
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NorthwindEntities db = new NorthwindEntities();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void valikMuutus(object sender, SelectionChangedEventArgs e)
        {
            this.griidike.ItemsSource =
                db.Customers.Where(x => x.Country == this.valik.SelectedItem.ToString())
                .ToList()
                ;
        }

        private void akenLaeti(object sender, RoutedEventArgs e)
        {
            this.valik.ItemsSource =
                db.Customers.Select(x => x.Country).Distinct().ToList();
        }
    }
}
