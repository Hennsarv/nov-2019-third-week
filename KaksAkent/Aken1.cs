﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KaksAkent
{
    public partial class Aken1 : Form
    {
        public Aken1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            (new Teine { Teade = this.textBox1.Text, CategoryName = this.textBox2.Text}).Show();
           
        }

        private void Aken1_Load(object sender, EventArgs e)
        {
            NorthwindEntities db = new NorthwindEntities();
            this.listBox1.DataSource = db.Categories.Select(x => x.CategoryName).ToList();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.textBox2.Text = this.listBox1.SelectedItem.ToString();
        }
    }
}
