﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Threading.Tasks;

namespace ConsoleApp84
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            NorthwindEntities ne = new NorthwindEntities();
            //ne.Database.Connection.Open();
            
            foreach (var p in ne.Products 
                .Include("Category") 
                //.ToListAsync().Result
                )
                Console.WriteLine($"{p.Category?.CategoryName}\t{p.ProductName}\t{p.UnitPrice}");
            


            foreach(var c in ne.Categories)
            {
                Console.WriteLine(c.CategoryName);
                foreach(var p in c.Products)
                    Console.WriteLine(p.ProductName+"\t"+p.Category.CategoryName);
            }

            //ne.Categories.Add(new Category { CategoryName = "Test" });
            //ne.SaveChanges();
        }
    }

    public class NorthwindEntities : DbContext
    {
        public NorthwindEntities()
            : base("Data Source=valiitsql.database.windows.net;Initial Catalog=NOrthwind;Persist Security Info=True;User ID=Student;Password=Pa$$w0rd")
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
    }

    public class Category
    {
        public Category()
            {
                Products = new HashSet<Product>();
        }

        [Key][DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public byte[] Picture { get; set; }

        //[ForeignKey("CategoryId")]
        public ICollection<Product> Products { get; set; }
        
    }

    public class Product
    {
        [Key][DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public Nullable<int> SupplierID { get; set; }
        [ForeignKey("Category")]
        public Nullable<int> CategoryID { get; set; }
        public string QuantityPerUnit { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public Nullable<short> UnitsInStock { get; set; }
        public Nullable<short> UnitsOnOrder { get; set; }
        public Nullable<short> ReorderLevel { get; set; }
        public bool Discontinued { get; set; }

        //[ForeignKey("Category")]
        public virtual Category Category { get; set; }

    }
}
