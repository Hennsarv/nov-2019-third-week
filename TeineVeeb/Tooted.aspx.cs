﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TeineVeeb
{
    public partial class Tooted : System.Web.UI.Page
    {
        NorthwindEntities db = new NorthwindEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                string cat = this.valik.SelectedItem.ToString();
                griidike.DataSource = db.Products
                    .Where(p => p.Category.CategoryName == cat)
                    .ToList();
                griidike.DataBind();
            } else
            {
                valik.DataSource = db.Categories.Select(x => x.CategoryName).ToList();
                valik.DataBind();

                griidike.DataSource = db.Products

                    .ToList();
                griidike.DataBind();
            }
        }

        protected void valik_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        protected void griidike_Load(object sender, EventArgs e)
        {

        }
    }
}