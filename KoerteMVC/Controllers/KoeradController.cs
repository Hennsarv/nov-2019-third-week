﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using F = System.IO;
using Newtonsoft.Json;
using KoerteMVC.Models;




namespace KoerteMVC.Controllers
{

    static class E
    {
        // selle tegin et näidata, mis on Listi ForEach meetod
        public static void MinuForeach<T>(this IEnumerable<T> asjad, Action<T> b)
        {
            foreach (var asi in asjad) b(asi);
        }
    }

    public class KoeradController : Controller
    {
        string KoeradFile;

        

        public void LoeKoerad() // oi ma maadlesin siin!
        {
            KoeradFile = Server.MapPath("~/App_Data/Koerad.json");


            JsonConvert.DeserializeObject<List<Koer>>(F.File.ReadAllText(KoeradFile))
               .ForEach(x => x.Add());
        }

        

        public void SalvestaKoerad()
        {
            KoeradFile = Server.MapPath("~/App_Data/Koerad.json");
            F.File.WriteAllText(KoeradFile,
            JsonConvert.SerializeObject(Koer.Koerad));
        }

        // GET: Koerad
        public ActionResult Index()
        {
            LoeKoerad();
            return View(Koer.Koerad);
        }

        // GET: Koerad/Details/5
        public ActionResult Details(int id)
        {
            if (Koer.Koerad.Count() == 0) LoeKoerad();
            return View(Koer.Find(id));
        }

        // GET: Koerad/Create
        public ActionResult Create()
        {
            if (Koer.Koerad.Count() == 0) LoeKoerad();
            return View();
        }

        // POST: Koerad/Create
        [HttpPost]
        public ActionResult Create(Koer koer) //FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                koer.Add();
                SalvestaKoerad();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Koerad/Edit/5
        public ActionResult Edit(int id)
        {
            if (Koer.Koerad.Count() == 0) LoeKoerad();
            return View(Koer.Find(id));
        }

        // POST: Koerad/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Koer uusKoer)
        {
            try
            {
                // TODO: Add update logic here
                Koer vanaKoer = Koer.Find(id);
                if (vanaKoer != null)
                {
                    vanaKoer.Nimi = uusKoer.Nimi;
                    vanaKoer.Toug = uusKoer.Toug;
                }
                SalvestaKoerad();


                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Koerad/Delete/5
        public ActionResult Delete(int id)
        {
            if (Koer.Koerad.Count() == 0) LoeKoerad();
            return View(Koer.Find(id));
        }

        // POST: Koerad/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                Koer.Find(id)?.Remove();
                SalvestaKoerad();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
