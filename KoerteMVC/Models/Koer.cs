﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KoerteMVC.Models
{
    

    public class Koer
    {
        static Dictionary<int,Koer> _Koerad = new Dictionary<int, Koer>();
        public static IEnumerable<Koer> Koerad => _Koerad.Values;
        static int nr = 0;
        public int Id { get; set; } = ++nr;
        public string Nimi { get; set; }
        public string Toug { get; set; }


        public static Koer Find(int id) => _Koerad.ContainsKey(id) ? _Koerad[id] : null;

        public void Add()
        {
            if (!_Koerad.ContainsKey(this.Id)) _Koerad.Add(this.Id, this);

            nr = _Koerad.Keys.Max();
        }

        public void Remove() => _Koerad.Remove(this.Id);
        public static void Remove(int id) => _Koerad.Remove(id);
    }
}