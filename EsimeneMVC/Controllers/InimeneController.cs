﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EsimeneMVC.Models;

namespace EsimeneMVC.Controllers
{
    public class InimeneController : Controller
    {
        // GET: Inimene
        public ActionResult Index()
        {
            
            return View(Inimene.Inimesed);
        }

        // GET: Inimene/Details/5
        public ActionResult Details(int id)
        {
            Inimene kes = Inimene.Find(id);
            if (kes == null) return HttpNotFound();
            return View(kes);
        }

        // GET: Inimene/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimene/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                if ((string)collection["Nimi"] == "") RedirectToAction("Index");
                // TODO: Add insert logic here

                Inimene.Inimesed.Add(
                    new Inimene
                    {
                        Nimi = collection["Nimi"].ToString(),
                        Vanus = int.TryParse(collection["Vanus"], out int v) ? v : 0
                    }
                    ) ; 

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Edit/5
        public ActionResult Edit(int? id)
        {
            return View(Inimene.Find(id??0)) ;
        }

        // POST: Inimene/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {


            try
            {
                Inimene kes = Inimene.Find(id);
                if (kes == null) return HttpNotFound();

                // TODO: Add update logic here
                kes.Nimi = (string)collection["Nimi"];
                kes.Vanus = int.Parse(collection["Vanus"]);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Delete/5
        public ActionResult Delete(int id)
        {
            Inimene kes = Inimene.Find(id);
            if (kes == null) RedirectToAction("Index");
            return View(kes);
        }

        // POST: Inimene/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Inimene kes = Inimene.Find(id);
                if (kes != null) Inimene.Inimesed.Remove(kes);
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
