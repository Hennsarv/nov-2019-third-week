﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIsOnEvent
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene { Nimi = "Henn", Vanus = 64 };

            henn.Juubel += (Inimene x) => Console.WriteLine($"Saab pidu sest {x.Nimi} saab {x.Vanus} aastaseks");
            henn.Juubel += (Inimene x) => Console.WriteLine($"peaks sellele tüübile {x.Nimi} kingitusre ostma");
            Console.WriteLine(henn);

            for (int i = 0; i < 30; i++)
            {
                henn.Vanus++;
            }
            Console.WriteLine(henn);

        }
    }

    class Inimene
    {
        public event Action<Inimene> Juubel = null;

        public string Nimi;
        private int _Vanus;
        public int Vanus
        {
            get => _Vanus;
            set
            {
                _Vanus = value;
                if(_Vanus % 25 == 0)
                {
                    if (Juubel != null) Juubel(this);
                }
            }
        } 
            
        public override string ToString() => $"Inimene {Nimi} on {Vanus} aastane";



        
    }
}
