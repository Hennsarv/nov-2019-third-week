﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsimeneAken
{
    public class Tulemus
    {
        public string Nimi { get; set; }
        public int Distants { get; set; }
        public int Aeg { get; set; }
        public double Kiirus => Distants * 1.0 / Aeg;
    }
}
