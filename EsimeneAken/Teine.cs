﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsimeneAken
{
    partial class Loom
    {
        public override string ToString() => $"Loom {Nimi} liigist {Liik}";
        
    }
}
