﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsimeneAken
{
    public class Inimene
    {
        public string EesNimi { get; set; }
        public string PereNimi { get; set; }
        public int Vanus { get; set; }

        public int Palk { get; set; }
    }
}
