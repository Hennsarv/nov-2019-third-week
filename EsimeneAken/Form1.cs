﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace EsimeneAken
{
    public partial class Form1 : Form
    {
        List<Inimene> Inimesed = new List<Inimene>
        {
            new Inimene {EesNimi="Henn", PereNimi="Sarv", Vanus = 64},
            new Inimene {EesNimi="Ants", PereNimi="Saunamees", Vanus = 40},
            new Inimene {EesNimi="Peeter", PereNimi="Suur", Vanus = 28},
        };

        public Form1()
        {
            InitializeComponent();
        }

        private void btnTere_Click(object sender, EventArgs e)
        {
            this.lblHello.Text = $"Tere {this.txtNimi.Text}!";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = Inimesed;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Inimesed.Add(new Inimene
            {
                EesNimi = this.textBox1.Text,
                PereNimi = this.textBox2.Text,
                Vanus = int.TryParse(this.textBox3.Text, out int vanus) ? vanus : 0
            }

                ) ;
            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = Inimesed;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            System.IO.File.WriteAllText("..\\..\\inimesed.json"
                , JsonConvert.SerializeObject(Inimesed)
                ) ;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Inimesed = JsonConvert.DeserializeObject<List<Inimene>>(
                System.IO.File.ReadAllText("..\\..\\inimesed.json")
                );

            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = Inimesed;

        }
    }
}
