﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EsimeneAken
{
    public partial class Protokoll : Form
    {
        List<Tulemus> tulemused = null;
        public Protokoll()
        {
            InitializeComponent();
        }

        private void Protokoll_Load(object sender, EventArgs e)
        {
            tulemused = System.IO.File.ReadAllLines("..\\..\\spordipäeva protokoll.txt")
                .Skip(1)
                .Select(x => x.Split(','))
                .Select(x => new Tulemus
                {
                    Nimi = x[0].Trim(),
                    Distants = int.TryParse(x[1], out int d) ? d : 0,
                    Aeg = int.TryParse(x[2], out int a) ? a : 0
                }).ToList();

            this.comboBox1.DataSource = (new List<string> { "kõik" }).Union(tulemused.Select(x => x.Nimi).Distinct()).ToList(); ;
            this.textBox1.Text = "kõik";
            }

        private void button1_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = this.textBox1.Text == "kõik" ? tulemused
                : tulemused.Where(x => x.Nimi == this.textBox1.Text).ToList();
            this.dataGridView1.Columns[3].DefaultCellStyle.Format = "F2";
            this.dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string nimi = (string)this.comboBox1.SelectedItem;
            this.textBox1.Text = nimi;
            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = 
                this.dataGridView1.DataSource = nimi == "kõik" ? tulemused
                : tulemused.Where(x => x.Nimi == nimi).ToList();
            this.dataGridView1.Columns[3].DefaultCellStyle.Format = "F2";
            this.dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }
    }
}
